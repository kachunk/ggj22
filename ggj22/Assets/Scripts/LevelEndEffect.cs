using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEndEffect : Effect
{
    public override void OnActivate() {
        Game.game.EndLevel();
    }
}
