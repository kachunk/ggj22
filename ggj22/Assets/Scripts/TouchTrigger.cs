using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchTrigger : Trigger
{
    private void OnTriggerEnter2D(Collider2D other) {
        TriggerStart();
    }

    private void OnCollisionEnter2D(Collision2D other) {
        TriggerStart();
    }

    private void OnTriggerExit2D(Collider2D other) {
        TriggerEnd();
    }

    private void OnCollisionExit2D(Collision2D other) {
        TriggerEnd();
    }
}
