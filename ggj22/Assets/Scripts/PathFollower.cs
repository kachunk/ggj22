using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;

public class PathFollower : MonoBehaviour
{
    public PathCreator pathCreator;
    public EndOfPathInstruction end;
    public float speed;
    public bool rotate;
    public float forwardAngle = 0;
    public float dstTravelled;
    Rigidbody2D rb;

    private void Start() {
        rb = gameObject.GetComponent<Rigidbody2D>();
    }
    void FixedUpdate()
    {
        dstTravelled += speed * Time.deltaTime;
        var nextPoint = pathCreator.path.GetPointAtDistance(dstTravelled, end);
        rb.velocity = nextPoint - transform.position;
        if(rotate) {
            transform.right = rb.velocity;
            if(forwardAngle != 0) {
                transform.Rotate(new Vector3(0, 0, forwardAngle));
            }
        }
    }
}
