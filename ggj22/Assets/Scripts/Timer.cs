using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : Trigger
{
    
    public float closeTime = 2.0f;
    [SerializeField()]
    private float remainTime = 0;
    private bool isClosed = false;

    private void Update()
    {
        if (remainTime > closeTime)
        {
            TriggerStart();
            isClosed = true;
        } else if (remainTime < 0)
        {
            TriggerEnd();
            isClosed = false;
        }

        if (!isClosed) remainTime += Time.deltaTime;
        else remainTime -= Time.deltaTime;
    }
}
