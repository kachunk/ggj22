using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour
{
    public bool active = false;
    public bool invert = false;
    public bool persistent;
    
    public void TriggerStart() {
        active = !invert;
    }

    public void TriggerEnd() {
        if(!persistent) {
            active = invert;
        }
    }
}
