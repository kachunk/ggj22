using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    private bool isPaused = false;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.JoystickButton7))
        {
            PauseGame();
        }
    }

    private void TrueState()
    {
        transform.GetChild(0).gameObject.SetActive(true);
        isPaused = true;
    }
    public void FalseState()
    {
        transform.GetChild(0).gameObject.SetActive(false);
        isPaused = false;
    }


        //Pause Game Function
        public void PauseGame()
    {
        if (!isPaused)
        {
            Game.game.Pause();
            TrueState();
        } else
        {
            Game.game.Continue();
            FalseState();
        }
    }

    //Menu Button Functions
    public void BackToMenu()
    {
        SceneManager.LoadScene(0);
    }
}
