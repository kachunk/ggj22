using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPositionFixer : MonoBehaviour
{
    public GameObject targetPosition;

    // Update is called once per frame
    void Update()
    {
        transform.position = targetPosition.transform.position;
        transform.rotation = targetPosition.transform.rotation;
    }
}
