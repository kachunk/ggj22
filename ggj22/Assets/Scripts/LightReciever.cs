using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LightReciever : Trigger
{
    public GameObject[] emitters;
    int mask;
    GameObject myCollider;
    // Start is called before the first frame update
    void Start()
    {
        if(emitters == null || emitters.Length == 0) {
            emitters = GameObject.FindObjectsOfType<LightEmitter>().Select(x => x.gameObject).ToArray<GameObject>();
        }
        mask = LayerMask.GetMask("wall", "lightwall", "feeshandlight", LayerMask.LayerToName(gameObject.layer));
        myCollider = gameObject.GetComponentInParent<Collider2D>().gameObject;
    }

    private void FixedUpdate() {
        foreach (var emitter in emitters)
        {
            Vector2 direction = transform.position - emitter.transform.position;
            var wallhit = Physics2D.Raycast(
                emitter.transform.position,
                direction, 
                direction.magnitude, 
                mask);
            if(wallhit.transform != null && wallhit.transform.gameObject.layer == myCollider.layer) {
                if(active == invert) {
                    TriggerStart();
                }
            } else {
                if(active != invert) {
                    TriggerEnd();
                }
            }
        }
    }
}
