using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SharkAi : Effect
{
    PathFollower pf;
    public bool followMode = false;
    public GameObject target;
    Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        pf = gameObject.GetComponent<PathFollower>();
        rb = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        if(followMode) {
            rb.velocity = (target.transform.position - transform.position).normalized * pf.speed;
        }
    }

    public override void OnActivate()
    {
        pf.enabled = false;
        followMode = true;
    }
    public override void OnDeactivate()
    {
        pf.enabled = true;
        followMode = false;
    }
}
