using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpenEffect : Effect
{
    public GameObject openObject;
    public GameObject closedObject;
    public override void OnActivate() {
        if(closedObject != null) {
            closedObject.SetActive(false);
        }
        if(openObject != null) {
            openObject.SetActive(true);
        }
    }

    public override void OnDeactivate() {
        if(closedObject != null) {
            closedObject.SetActive(true);
        }
        if(openObject != null) {
            openObject.SetActive(false);
        }
    }
}
