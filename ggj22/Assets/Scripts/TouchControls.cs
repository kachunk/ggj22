﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class TouchControls : MonoBehaviour
{
    private int touchId = -1;
    public Vector2 touchStart = Vector2.zero;
    public Vector2 touchPos = Vector2.up * 1000f;
    public float tapTimeout = 0.3f;
    public float tapMaxDistance = 30f;
    private float tapMaxDistanceSquared;
    public float stickRadius = 100f;
    public float deadzone = 10f;
    private float deadzoneSquared;
    private RectTransform touchArea;
    public bool onlyX = false;
    private float touchStartTime = 0;
    private bool wasTapped = false;
    public Image stickCircle;
    public Image stickNub;

    public bool isTouched()
    {
        return touchId != -1;
    }

    public bool WasTapped()
    {
        return wasTapped;
    }

    public Vector2 getJoystickPosition()
    {
        if(touchId == -1)
        {
            return Vector2.zero;
        } else
        {
            Vector2 res = (touchPos - touchStart);
            if(res.sqrMagnitude < deadzoneSquared)
            {
                return Vector2.zero;
            }
            res *= (1f / stickRadius);
            if (onlyX)
            {
                res.y = 0;
            }
            if (res.sqrMagnitude > 1f)
            {
                res.Normalize();
            }
            return res;
        }
    }

    void Start()
    {
        touchArea = GetComponent<RectTransform>();
        deadzoneSquared = deadzone * deadzone;
        tapMaxDistanceSquared = tapMaxDistance * tapMaxDistance;
    }

    // Update is called once per frame
    void Update()
    {
        wasTapped = false;
        for (int i = 0; i < Input.touchCount; ++i) {
            Touch t = Input.GetTouch(i);
            Vector2 posFixed;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(touchArea, t.position, null, out posFixed);
            if (t.phase == TouchPhase.Began)
            {
                if (!EventSystem.current.IsPointerOverGameObject(t.fingerId))
                {
                    if (touchArea.rect.Contains(posFixed))
                    {
                        if (touchId == -1)
                        {
                            touchId = t.fingerId;
                            touchStart = posFixed;
                            touchPos = posFixed;
                            touchStartTime = Time.timeSinceLevelLoad;
                            stickCircle.rectTransform.anchoredPosition = touchStart;
                            stickCircle.gameObject.SetActive(true);
                            stickNub.rectTransform.anchoredPosition = touchPos;
                            stickNub.gameObject.SetActive(true);
                        }
                    }
                }
            } else if(t.phase == TouchPhase.Canceled || t.phase == TouchPhase.Ended)
            {
                if(touchId == t.fingerId)
                {
                    touchId = -1;
                    if(Time.timeSinceLevelLoad - touchStartTime < tapTimeout && (touchStart - posFixed).sqrMagnitude < tapMaxDistanceSquared)
                    {
                        wasTapped = true;
                    }
                    stickCircle.gameObject.SetActive(false);
                    stickNub.gameObject.SetActive(false);
                }
            } else
            {
                if (touchId == t.fingerId)
                {
                    touchPos = posFixed;
                    stickNub.rectTransform.anchoredPosition = touchStart + (getJoystickPosition() * stickRadius);
                }
            }
        }
    }
}
