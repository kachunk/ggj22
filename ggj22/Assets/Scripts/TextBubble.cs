using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[ExecuteAlways]
public class TextBubble : MonoBehaviour
{
    public string text;
    TMP_Text tmptext;
    SpriteRenderer bubble;
    // Start is called before the first frame update
    void Start()
    {
        tmptext = gameObject.GetComponentInChildren<TMP_Text>();
        bubble = gameObject.GetComponentInChildren<SpriteRenderer>();
        SetText(text);
    }
    private void Update() {
        Start();
    }

    public void SetText(string txt) {
        this.text = txt;
        tmptext.SetText(txt);
        tmptext.ForceMeshUpdate();
        Vector2 textRect = tmptext.GetRenderedValues();
        bubble.size = textRect + new Vector2(.6f, .3f);
        bubble.transform.localPosition = bubble.size * 0.5f + new Vector2(0f, 0f);
    }
}
