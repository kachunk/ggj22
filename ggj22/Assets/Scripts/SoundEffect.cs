using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundEffect : Effect
{
    AudioSource sound;
    public AudioClip clip;
    public bool cancel = false;
    private void Start() {
        sound = GetComponent<AudioSource>();
    }
    public override void OnActivate()
    {
        sound.PlayOneShot(clip);
    }

    public override void OnDeactivate()
    {
        if(cancel) {
            sound.Stop();
        }
    }
}
