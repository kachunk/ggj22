using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D.Animation;

public class FeeshMover : MonoBehaviour
{
    public float maxAngle = 15f;
    public float rotationSpeed = 2f;
    public float flapDuration = 2f;
    public float flapIntensity = 0.2f;
    public float flapAdvancePerBone = -0.5f;
    public float currentFlapPhase = 0f;
    public float flapGain = 1.2f;
    Rigidbody2D rb;
    SpriteSkin ss;

    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponentInParent<Rigidbody2D>();
        ss = gameObject.GetComponentInParent<SpriteSkin>();
    }

    // Update is called once per frame
    void Update()
    {
        if(rb.velocity.sqrMagnitude > 0.1) {
            Vector2 lookAt = rb.velocity;
            float speed = rb.velocity.magnitude * Time.deltaTime * rotationSpeed;
            int i = 0;
            float angle = 0;
            foreach (var bone in ss.boneTransforms)
            {
                float rotAngleUnscaled = Vector2.SignedAngle(-bone.right, lookAt);
                float rotAngle = rotAngleUnscaled * speed;
                float flapAngle = Mathf.Sin(currentFlapPhase + i * flapAdvancePerBone) * flapIntensity * flapGain * i;
                if(rotAngleUnscaled * flapAngle < 0 || Mathf.Abs(rotAngleUnscaled / 20) < Mathf.Abs(flapAngle)) {
                    rotAngle -= flapAngle;
                }
                bone.Rotate(new Vector3(0, 0, rotAngle - angle));
                lookAt = -bone.right;
                angle = rotAngle;
                ++i;
                currentFlapPhase += Time.deltaTime * Mathf.PI * 2 / flapDuration;
            }
        }
    }
}
