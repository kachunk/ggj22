using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{

    public TouchControls touchControl;
    public string horizontalAxisName = "Horizontal";
    public string verticalAxisName = "Vertical";
    public float deadzone = 0.5f;
    private float deadzoneSquared;
    public Rigidbody2D controledCharacter;
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        deadzoneSquared = deadzone * deadzone;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 input = Vector2.zero;
        if(touchControl.isTouched()) {
            input = touchControl.getJoystickPosition();
        } else {
            var horizontal = Input.GetAxis(horizontalAxisName);
            var vertical = -Input.GetAxis(verticalAxisName);
            input = new Vector2(horizontal, vertical);
        }
        if(input.sqrMagnitude > deadzoneSquared) {
            controledCharacter.velocity = input * speed;
        } else {
            controledCharacter.velocity = Vector2.zero;
        }
    }
}
