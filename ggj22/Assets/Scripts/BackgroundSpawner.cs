using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundSpawner : MonoBehaviour
{
    public GameObject[] pool;
    public int count;
    public SpriteRenderer referenceTexture;

    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log("X: " + referenceTexture.transform.position.x);
        //Debug.Log("Y: " + referenceTexture.transform.position.y);

        float xLower = referenceTexture.transform.position.x-(referenceTexture.bounds.size.x/2);
        float yLower = referenceTexture.transform.position.y-(referenceTexture.bounds.size.y/2);
        float xUpper = xLower+referenceTexture.bounds.size.x;
        float yUpper = yLower+referenceTexture.bounds.size.y;

        //Debug.Log("X: " + xLower + ", " + xUpper);
        //Debug.Log("Y: " + yLower + ", " + yUpper);

        for (int i = 0; i < count; ++i) {
            spawnObject(xLower, xUpper, yLower, yUpper);
        }
    }

    void spawnObject(float xLower, float xUpper, float yLower, float yUpper) {
        int index = Random.Range(0, pool.Length);
        Vector2 relativePos = new Vector2(Random.Range(xLower, xUpper),
                                          Random.Range(yLower, yUpper));
        //Debug.Log("Spawned Location: " + relativePos);
        GameObject spawnedObject = Instantiate(pool[index], (Vector3) relativePos, Quaternion.identity);
        spawnedObject.transform.parent = transform;
    }
}
