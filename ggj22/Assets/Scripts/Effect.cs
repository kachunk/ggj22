using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effect : MonoBehaviour
{
    public Trigger[] triggers;
    public bool isAllActive = false;
    public bool isSingleActive = false;
    public bool multiactivate = false;

    public virtual void Update() {
        bool allActive = true;
        bool singleActive = false;
        foreach (var trigger in triggers)
        {
            if(!singleActive && trigger.active) {
                singleActive = true;
                if(!isSingleActive) {
                    isSingleActive = true;
                    OnSingleActivate();
                }
                OnSingleActive();
            }
            if(!trigger.active) {
                allActive = false;
            }
            if(!allActive && singleActive) {
                break;
            }
        }
        if(!singleActive && isSingleActive) {
            isSingleActive = false;
            OnSingleDeactivate();
        }
        if(allActive) {
            if(!isAllActive) {
                isAllActive = true;
                OnAllActivate();
            }
            OnAllActive();
        } else if(isAllActive) {
            isAllActive = false;
            OnAllDeactivate();
        }
    }

    public virtual void OnActivate() {
    }
    public virtual void OnActive() {
    }
    public virtual void OnDeactivate() {
    }
    public virtual void OnAllActivate() {
        OnActivate();
    }
    public virtual void OnAllActive() {
        OnActive();
    }
    public virtual void OnAllDeactivate() {
        OnDeactivate();
    }

    public virtual void OnSingleActivate() {
        if(!multiactivate) {
            OnActivate();
        }
    }
    public virtual void OnSingleActive() {
        if(!multiactivate) {
            OnActive();
        }
    }
    public virtual void OnSingleDeactivate() {
        if(!multiactivate) {
            OnDeactivate();
        }
    }
}
