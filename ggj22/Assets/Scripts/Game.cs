using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class Game : MonoBehaviour
{
    public GameObject killscreen;
    public GameObject winscreen;
    public TMP_Text deathReasonText;
    public GameObject[] inputControls;
    public static Game game;
    // Start is called before the first frame update
    void Start()
    {
        game = this;
        Time.timeScale = 1f;
    }

    public void Die(string reason) {
        Pause();
        deathReasonText.text = reason.Replace("  ", "\n");
        killscreen.SetActive(true);
    }

    public void Pause() {
        foreach (var input in inputControls)
        {
            input.SetActive(false);
        }
        Time.timeScale = 0;
    }

    public void Continue() {
        foreach (var input in inputControls)
        {
            input.SetActive(true);
        }
        Time.timeScale = 1;
    }

    public void Restart() {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
        Continue();
    }

    public void EndLevel() {
        Pause();
        winscreen.SetActive(true);
    }

    public void NextLevel() {
        Scene scene = SceneManager.GetActiveScene();
        var nextScene = Mathf.Min(scene.buildIndex + 1, SceneManager.sceneCountInBuildSettings - 1);
        SceneManager.LoadScene(nextScene);
        Continue();
    }

    private void Update() {
        if(Input.GetKeyDown(KeyCode.R) || Input.GetKeyDown(KeyCode.JoystickButton3)) {
            Restart();
        }
    }
}
