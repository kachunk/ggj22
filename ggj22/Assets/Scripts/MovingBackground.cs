﻿using UnityEngine;
using System.Collections;

public class MovingBackground : MonoBehaviour {

	public Camera cam;
	public int edgeCount = 3;
	public SpriteRenderer mainTexture;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		var distTarget = cam.transform.position - transform.position;
		var target = transform.position;
		var targetLeft = transform.position - new Vector3( edgeCount * mainTexture.bounds.size.x , 0, 0);
		var distLeft = cam.transform.position - targetLeft;
		if (distTarget.sqrMagnitude > distLeft.sqrMagnitude) {
			distTarget = distLeft;
			target = targetLeft;
		}
		var targetRight = transform.position + new Vector3( edgeCount * mainTexture.bounds.size.x , 0, 0);
		var distRight = cam.transform.position - targetRight;
		if (distTarget.sqrMagnitude > distRight.sqrMagnitude) {
			distTarget = distRight;
			target = targetRight;
		}
		var targetUp = transform.position - new Vector3( 0, edgeCount * mainTexture.bounds.size.y , 0);
		var distUp = cam.transform.position - targetUp;
		if (distTarget.sqrMagnitude > distUp.sqrMagnitude) {
			distTarget = distUp;
			target = targetUp;
		}
		var targetDown = transform.position + new Vector3( 0, edgeCount * mainTexture.bounds.size.y , 0);
		var distDown = cam.transform.position - targetDown;
		if (distTarget.sqrMagnitude > distDown.sqrMagnitude) {
			distTarget = distDown;
			target = targetDown;
		}
		transform.position = target;
	}
}
