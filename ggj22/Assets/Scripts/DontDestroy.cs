using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroy : MonoBehaviour
{
    // Start is called before the first frame update
    private void Awake()
    {
        GameObject[] objsMusic = GameObject.FindGameObjectsWithTag("music");
        GameObject[] objsOptions = GameObject.FindGameObjectsWithTag("menu");
        if ( objsMusic.Length > 1) Destroy(this.gameObject);
        if (objsOptions.Length > 1) Destroy(this.gameObject);
        DontDestroyOnLoad(this.gameObject);
    }
}
