using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillEffect : Effect
{
    public string killReason;
    public override void OnActivate() {
        Game.game.Die(killReason);
    }
}
