using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;
using UnityEngine.Audio;
using TMPro;
using Utilities;


public class MenuScript : MonoBehaviour
{
    public GameObject optionsButton;
    public GameObject exitButton;

    [Header("UI Fields")]
    public GameObject optionsUI;
    public GameObject levelsUI;
    public GameObject creditsUI;
    public GameObject mainUI;
    public GameObject activateAll;

    [Header("Graphics")]
    public TMP_Dropdown resolutionDropdown;
    Resolution[] resolutions;
    

    public TMP_Dropdown qualityDropdown;
    QualitySettings[] qualitySettings;

    [Header("Audio")]
    public AudioMixer mixer;

    private bool inOptions = false;

    [Header("LevelUI")]
    public TMP_Dropdown levelDropdown;
    public SceneField[] levelScenes;


    private void Awake()
    {
        mixer.SetFloat("MainVolume", -40f);
    }

    public void Start()
    {
        QualitySettings.SetQualityLevel(2);
        qualityDropdown.value = 2;

        resolutions = Screen.resolutions;

        resolutionDropdown.ClearOptions();

        List<string> options = new List<string>();

        int currentResolutionIndex = 0;
        for(int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + " x " + resolutions[i].height + " [" + resolutions[i].refreshRate + "Hz]";
            options.Add(option);

            if(resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }
        }

        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();


        //Refresh Level Dropdown
        levelDropdown.ClearOptions();
        List<string> levelOpts = new List<string>();

        int currentLevelIndex = 0;
        for (int i = 0; i < levelScenes.Length; i++)
        {
            string levelOption = levelScenes[i].SceneName.Replace(levelScenes[i].SceneName, "Level " + (i + 1));
            levelOpts.Add(levelOption);

            currentLevelIndex = i;
        }

        levelDropdown.AddOptions(levelOpts);
        levelDropdown.value = currentLevelIndex;
        levelDropdown.RefreshShownValue();


        if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.LinuxPlayer || Application.platform == RuntimePlatform.LinuxEditor)
        {
            optionsButton.SetActive(true);
            exitButton.SetActive(true);
        }
        else
        {
            optionsButton.SetActive(false);
            exitButton.SetActive(false);
        }
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (inOptions)
            {
                inOptions = false;

                CloseOptionsMenu();
            }
        }
    }

    //UI Panel Control
    public void OpenOptionsMenu()
    {
        mainUI.SetActive(false);
        optionsUI.SetActive(true);
        inOptions = true;

    }

    public void CloseOptionsMenu()
    {
        optionsUI.SetActive(false);
        mainUI.SetActive(true);

        inOptions = false;

        if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.LinuxPlayer || Application.platform == RuntimePlatform.LinuxEditor)
        {
            optionsButton.SetActive(true);
            exitButton.SetActive(true);
        }
        else
        {
            optionsButton.SetActive(false);
            exitButton.SetActive(false);
        }
    }

    public void OpenSelectMenu()
    {
        mainUI.SetActive(false);
        levelsUI.SetActive(true);
    }

    public void CloseSelectMenu()
    {
        levelsUI.SetActive(false);
        mainUI.SetActive(true);

        if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.LinuxPlayer || Application.platform == RuntimePlatform.LinuxEditor)
        {
            optionsButton.SetActive(true);
            exitButton.SetActive(true);
        }
        else
        {
            optionsButton.SetActive(false);
            exitButton.SetActive(false);
        }
    }

    public void OpenCreditsMenu()
    {
        mainUI.SetActive(false);
        creditsUI.SetActive(true);
    }

    public void CloseCreditsMenu()
    {
        creditsUI.SetActive(false);
        mainUI.SetActive(true);

        if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.LinuxPlayer || Application.platform == RuntimePlatform.LinuxEditor)
        {
            optionsButton.SetActive(true);
            exitButton.SetActive(true);
        }
        else
        {
            optionsButton.SetActive(false);
            exitButton.SetActive(false);
        }
    }

    //Buttons
    public void StartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void EndGame()
    {
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #endif
        Application.Quit();
    }



    //Options Menu

    public void SetFullscreen()
    {
        Screen.fullScreen = !Screen.fullScreen;
    }

    public void ActivateResolution()
    {
        Resolution resolution = resolutions[resolutionDropdown.value];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    public void ChangeGraphicSettings()
    {
        int qualityIndex = qualityDropdown.value;
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    public void SetVolume(System.Single vol)
    {
        mixer.SetFloat("MainVolume", vol);
    }

    //Levels Menu
    public void StartLevel()
    {
        SceneManager.LoadScene(levelDropdown.value+1);
    }
}
