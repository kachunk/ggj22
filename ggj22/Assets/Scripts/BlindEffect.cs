using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlindEffect : Effect
{
    public string killReason;
    public SpriteRenderer blinder;
    public float blindingDuration = 2f;
    public float unblindingDuration = 1f;
    public float activationTime = float.MaxValue;
    public float deactivationTime = float.MaxValue;
    public Color blindingColor = new Color(1f, 1f, 1f);
    public override void OnActivate() {
        deactivationTime = float.MaxValue;
        activationTime = Time.time;
        blinder.color = new Color(1f, 1f, 1f, 0f);
        blinder.gameObject.SetActive(true);
    }

    public override void OnDeactivate() {
        activationTime = float.MaxValue;
        deactivationTime = Time.time - ((1f - blinder.color.a) * unblindingDuration);
    }

    public override void Update() {
        base.Update();
        float effecttime = Time.time - activationTime;
        if(effecttime > 0) {
            blinder.color = new Color(blindingColor.r, blindingColor.g, blindingColor.b, Mathf.Clamp(effecttime / blindingDuration, 0f, 1f));
        }
        if(effecttime > blindingDuration) {
            Game.game.Die(killReason);
        }
        effecttime = Time.time - deactivationTime;
        if(effecttime > 0) {
            blinder.color = new Color(blindingColor.r, blindingColor.g, blindingColor.b, 1f - Mathf.Clamp(effecttime / unblindingDuration, 0f, 1f));
        }
        if(effecttime > unblindingDuration) {
            blinder.gameObject.SetActive(false);
            deactivationTime = float.MaxValue;
        }
    }

}
